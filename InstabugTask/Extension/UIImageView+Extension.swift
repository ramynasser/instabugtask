//
//  UIimageView+Extension.swift
//  InstabugTask
//
//  Created by Ramy Nasser on 2/21/19.
//  Copyright © 2019 Ramy Nasser. All rights reserved.
//

import UIKit
extension UIImageView {
    public func imageFromURL(urlString: String) {
        let activityIndicator = UIActivityIndicatorView(style: .gray)
        activityIndicator.frame = CGRect(x: 0, y: 0, width: frame.size.width, height: frame.size.height)
        activityIndicator.startAnimating()
        if image == nil {
            addSubview(activityIndicator)
        }

        URLSession.shared.dataTask(with: NSURL(string: urlString)! as URL, completionHandler: { (data, _, error) -> Void in

            if error != nil {
                print(error ?? "No Error")
                return
            }
            DispatchQueue.main.async(execute: { () -> Void in
                let image = UIImage(data: data!)
                activityIndicator.removeFromSuperview()
                self.image = image
            })

        }).resume()
    }
}
