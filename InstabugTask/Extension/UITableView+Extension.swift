//
//  UITableView+Extension.swift
//  InstabugTask
//
//  Created by Ramy Nasser on 2/21/19.
//  Copyright © 2019 Ramy Nasser. All rights reserved.
//

import UIKit
extension UITableView {
    func addInfiniteScroll(completion: (() -> Void)?) {
        let spinner = UIActivityIndicatorView(style: .gray)
        spinner.startAnimating()
        spinner.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: bounds.width, height: CGFloat(44))
        tableFooterView = spinner
        tableFooterView?.isHidden = false
        completion?()
    }
}
