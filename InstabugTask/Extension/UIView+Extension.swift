//
//  UIView+Extension.swift
//  InstabugTask
//
//  Created by Ramy Nasser on 2/20/19.
//  Copyright © 2019 Ramy Nasser. All rights reserved.
//

import Foundation
import UIKit

extension UIView {
    /*
     Show view Shadow ,
     actually add shadow layout to view

     @param shadowRadius - radius of shadow added to view
     @param shadowOpacity - opacity of shadow added to view
     */
    func dropCardShadow(shadowRadius: CGFloat? = nil, shadowOpacity: Float? = nil) {
        clipsToBounds = false
        layer.masksToBounds = false
        layer.shadowColor = UIColor.black.cgColor
        layer.shadowOpacity = shadowOpacity ?? 0.3
        layer.shadowOffset = CGSize(width: 0, height: 3)
        layer.shadowRadius = shadowRadius ?? 2
    }

    func removeCardShadow() {
        layer.shadowColor = UIColor.clear.cgColor
        layer.shadowOpacity = 0
        layer.shadowOffset = CGSize(width: 0, height: 0)
        layer.shadowRadius = 0
    }
}
