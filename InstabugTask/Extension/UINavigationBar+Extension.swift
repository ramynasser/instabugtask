//
//  UINavigationBar+Extension.swift
//  InstabugTask
//
//  Created by Ramy Nasser on 2/20/19.
//  Copyright © 2019 Ramy Nasser. All rights reserved.
//

import UIKit

extension UINavigationBar {
    enum NavigationBarStyle {
        case transparent
        case transparentWithShadowLine
        case solid
        case solidNoShadow
        case translucent
    }

    // NOTE: color will only be applied to solid and translucent styles
    func setStyle(style: NavigationBarStyle,
                  tintColor: UIColor = .white,
                  forgroundColor: UIColor = .white,
                  backgroundColor: UIColor = .white) {
        switch style {
        case .transparent:
            setBackgroundImage(UIImage(), for: UIBarMetrics.default)
            shadowImage = UIImage()
            isTranslucent = true
            self.backgroundColor = .clear
            self.tintColor = tintColor

        case .transparentWithShadowLine:
            setBackgroundImage(UIImage(), for: UIBarMetrics.default)
            shadowImage = nil
            isTranslucent = true
            self.tintColor = tintColor
            self.backgroundColor = .clear
        case .solid:
            setBackgroundImage(nil, for: UIBarMetrics.default)
            shadowImage = nil
            isTranslucent = false
            self.tintColor = tintColor
            barTintColor = forgroundColor
        case .solidNoShadow:
            setBackgroundImage(UIImage(), for: UIBarMetrics.default)
            shadowImage = UIImage()
            isTranslucent = false
            self.tintColor = tintColor
            barTintColor = forgroundColor
        case .translucent:
            setBackgroundImage(nil, for: UIBarMetrics.default)
            shadowImage = nil
            isTranslucent = true
            self.tintColor = tintColor
            self.backgroundColor = backgroundColor
            barTintColor = forgroundColor
        }
    }

    func setTitleColor(color: UIColor) {
        titleTextAttributes = [NSAttributedString.Key.foregroundColor: color]
    }
}
