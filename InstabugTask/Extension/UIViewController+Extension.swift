//
//  UIViewController+Extension.swift
//  InstabugTask
//
//  Created by Ramy Nasser on 2/20/19.
//  Copyright © 2019 Ramy Nasser. All rights reserved.
//

import UIKit
extension UIViewController {
    var identifier: String {
        return String(describing: type(of: self))
    }

    class var identifier: String {
        return String(describing: self)
    }

    func dissmissKeyboardWhenTappedAround() {
        view.endEditing(true)
    }

    func setupBackButton() {
        navigationController?.navigationBar.backIndicatorImage = #imageLiteral(resourceName: "back")
        navigationController?.navigationBar.backIndicatorTransitionMaskImage = #imageLiteral(resourceName: "back")
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        navigationItem.backBarButtonItem?.title = ""
    }

    func hideBackButton() {
        navigationItem.setHidesBackButton(true, animated: true)
    }

    func showBackButton() {
        navigationController?.isNavigationBarHidden = false
        navigationItem.setHidesBackButton(false, animated: true)
    }
}
