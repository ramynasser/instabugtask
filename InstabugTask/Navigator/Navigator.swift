//
//  Navigator.swift
//  InstabugTask
//
//  Created by Ramy Nasser  on 9/16/18.
//  Copyright © 2018 Ramy Nasser. All rights reserved.
//

import Foundation
import UIKit
enum NavigationTarget {
    case listMovie
    case addMovie
    var viewController: UIViewController? {
        var viewController: UIViewController?
        switch self {
        case .listMovie:
            let vc: UIViewController? = getViewController(with: MovieViewController.identifier, storyboardName: .movie)
            viewController = vc
        case .addMovie:
            let vc: UIViewController? = getViewController(with: AddMovieController.identifier, storyboardName: .addMovie)
            viewController = vc
        }
        return viewController
    }

    func getViewController<VC: UIViewController>(with identifier: String, storyboardName: Storyboards.Storyboard) -> VC? {
        return NavigationTarget.getViewController(with: identifier, storyboardName: storyboardName)
    }

    fileprivate static func getViewController<VC: UIViewController>(with identifier: String, storyboardName: Storyboards.Storyboard) -> VC? {
        let storyboard = Storyboards.get(storyboardName)
        return storyboard.instantiateViewController(withIdentifier: identifier) as? VC
    }
}

class Navigator {
    enum BackTarget {
        case overlay
        case controller
        case root
        case undetermined
    }

    static var topViewControllerViewType: Navigator.BackTarget = .undetermined
    static var rootViewController = UIApplication.shared.keyWindow?.rootViewController
    class func navigate(to navigationTarget: NavigationTarget, from rootController: UIViewController) {
        if let destinationVC = navigationTarget.viewController {
            topViewControllerViewType = .controller
            DispatchQueue.main.async {
                rootController.navigationController?.pushViewController(destinationVC, animated: true)
            }
        }
    }

    /**
     clearNavigationStack will remove all navigation controller unitel
     **/
    class func navigate(to navigationTarget: NavigationTarget, from rootNavigationTarget: NavigationTarget, clearNavigationStack: Bool = false) {
        if let destinationVC = navigationTarget.viewController {
            topViewControllerViewType = .controller
            DispatchQueue.main.async {
                if let sourceVC = rootNavigationTarget.viewController {
                    sourceVC.navigationController?.pushViewController(destinationVC, animated: true)
                }
            }
            if clearNavigationStack {}
        }
    }

    class func navigateToHome(from _: UIViewController) {}

    class func setRoot(target navigationTarget: NavigationTarget) {
        var window = AppDelegate.instance.window
        if window == nil {
            window = UIWindow(frame: UIScreen.main.bounds)
        }
        window?.rootViewController?.presentedViewController?.dismiss(animated: false, completion: nil)
        if let viewController = navigationTarget.viewController {
            topViewControllerViewType = .controller
            window?.rootViewController = UINavigationController(rootViewController: viewController)
            rootViewController = viewController
            window?.makeKeyAndVisible()
        }
    }

    class func clearNavigationStack(before navigationTarget: NavigationTarget, from soucreVC: UIViewController) {
        let vcs = soucreVC.navigationController?.viewControllers
        for (_, vc) in (vcs?.enumerated())! {
            if vc == navigationTarget.viewController { continue }
            else {
                soucreVC.navigationController?.popViewController(animated: false)
            }
        }
    }

    private class func clearNavigationStack(beforeVC: UIViewController) {
        let vcs = rootViewController?.navigationController?.viewControllers ?? []
        for (_, vc) in vcs.enumerated().reversed() {
            if vc == beforeVC {
                continue
            } else {
                rootViewController?.navigationController?.popViewController(animated: false)
            }
        }
    }

    class func present(target: NavigationTarget, from sourceViewController: UIViewController? = nil, completion: (() -> Void)? = nil, topToBottom: Bool = false) {
        guard let destinationVC = target.viewController else {
            return
        }

        topViewControllerViewType = .overlay

        if topToBottom {
            if let transitioningDelegate = sourceViewController as? UIViewControllerTransitioningDelegate {
                destinationVC.transitioningDelegate = transitioningDelegate
            }
        }

        if let sourceVC = sourceViewController {
            sourceVC.present(destinationVC, animated: true, completion: completion)
        } else {
            if var topController = UIApplication.shared.keyWindow?.rootViewController {
                while let presentedViewController = topController.presentedViewController {
                    topController = presentedViewController
                }
                topController.present(destinationVC, animated: true, completion: completion)
            }
        }
    }

    class func get(target: NavigationTarget, completion _: (() -> Void)? = nil) -> UIViewController? {
        guard let destinationVC = target.viewController else {
            return nil
        }
        return destinationVC
    }
}
