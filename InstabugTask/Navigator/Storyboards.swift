//
//  Storyboards.swift
//  InstabugTask
//
//  Created by Ramy Nasser 2018 on 9/16/18.
//  Copyright © 2018 Ramy Nasser. All rights reserved.
//

import Foundation
import UIKit

struct Storyboards {
    enum Storyboard {
        case movie
        case addMovie
    }

    static func get(_ storyboard: Storyboard) -> UIStoryboard {
        switch storyboard {
        case .movie:
            return UIStoryboard(name: "Movie", bundle: nil)
        case .addMovie:
            return UIStoryboard(name: "AddMovie", bundle: nil)
        }
    }
}
