//
//  MovieCell.swift
//  InstabugTask
//
//  Created by Ramy Nasser on 2/20/19.
//  Copyright © 2019 Ramy Nasser. All rights reserved.
//

import UIKit

class MovieCell: BaseTableCell {
    class var ReuseIdentifier: String { return "org.instabug.identifier.\(type(of: self))" }

    @IBOutlet var movieImg: UIImageView!
    @IBOutlet var overviewLbl: UILabel!
    @IBOutlet var dateLbl: UILabel!
    @IBOutlet var titleLbl: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    override func setupCellViews() {
        super.setupCellViews()
    }

    override func prepareForReuse() {
        super.prepareForReuse()
        movieImg.layer.removeAllAnimations()
        movieImg.image = nil
    }
}

extension MovieCell: TableCellDataSource {
    typealias Model = Movie
    func configureCell(with model: Movie?) {
        titleLbl.text = model?.title
        overviewLbl.text = model?.overview
        dateLbl.text = model?.releaseDate

        if let posterPath = model?.posterPath, model?.movieType == .allMovie {
            movieImg.imageFromURL(urlString: Urls.environment.mediaUrl.appending(posterPath))
        } else if let posterPath = model?.posterPath, model?.movieType == .myMovie {
            DispatchQueue.global(qos: .userInitiated).async {
                self.movieImg.image = Utlis.getSavedImage(named: posterPath)
            }
        } else {
            movieImg.image = #imageLiteral(resourceName: "placeholder")
        }
    }
}
