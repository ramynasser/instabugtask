//
//  UITableViewCellDelegate.swift
//  InstabugTask
//
//  Created by Ramy Nasser on 2/20/19.
//  Copyright © 2019 Ramy Nasser. All rights reserved.
//

import UIKit
protocol TableCellDelegate {
    static var identifier: String { get }
    static func nib() -> UINib
    func setupCellViews()
}
