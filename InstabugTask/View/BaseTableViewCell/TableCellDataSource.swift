//
//  TableCellDataSource.swift
//  InstabugTask
//
//  Created by Ramy Nasser on 2/20/19.
//  Copyright © 2019 Ramy Nasser. All rights reserved.
//

import Foundation
import UIKit
protocol TableCellDataSource {
    associatedtype Model
    func configureCell(with model: Model?)
}
