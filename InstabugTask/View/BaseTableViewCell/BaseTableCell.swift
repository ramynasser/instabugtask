//
//  BaseTableCell.swift
//  InstabugTask
//
//  Created by Ramy Nasser on 2/20/19.
//  Copyright © 2019 Ramy Nasser. All rights reserved.
//

import Foundation
import UIKit

class BaseTableCell: UITableViewCell, TableCellDelegate {
    override func awakeFromNib() {
        super.awakeFromNib()
        setupCellViews()
        // Initialization code
    }

    static var identifier: String {
        return className
    }

    static func nib() -> UINib {
        return UINib(nibName: className, bundle: nil)
    }

    func setupCellViews() {}
}
