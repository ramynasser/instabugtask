//
//  MoviePresenter.swift
//  InstabugTask
//
//  Created by Ramy Nasser on 2/20/19.
//  Copyright © 2019 Ramy Nasser. All rights reserved.
//

import UIKit

// MARK: - presenter view Protocol

protocol MoviePresenterProtocol: BaseViewProtocol {
    func refreshView()
    func addNoDataView()
    func showNoInternet()
}

// MARK: - presenter Protocol

protocol MovieView {
    func fetchMovies()
    func fetchLocalMovies()
    var localMovies: [Movie] { get }
    var localMovieCount: Int { get }
    func getLocalMovie(at index: Int) -> Movie
    var movies: [Movie] { get }
    var movieCount: Int { get }
    var currentPage: Int { get set }
    func getMovie(at index: Int) -> Movie
    func getSectionTitle(at section: Int) -> String
    func getNumberOfRowsInSection(at section: Int) -> Int
    func getMovieForCell(at indexPath: IndexPath) -> Movie
    func getNumberOfSection() -> Int
    func loadMoreMovies(for tableView: UITableView, forRowAt indexPath: IndexPath)
}

class MoviePresenter: BasePresenter, MovieView {
    private var movieService: MovieGatewayProtocol?
    private var coreDataMoviesGateway: LocalPersistenceMoviesGateway?
    private weak var movieView: MoviePresenterProtocol?
    private weak var moviePresenterProtocol: MoviePresenterProtocol? {
        if movieView == nil {
            movieView = getView() as? MoviePresenterProtocol
        }
        return movieView
    }

    var movies: [Movie] = []
    var localMovies: [Movie] = []
    var localMovieCount: Int {
        return localMovies.count
    }

    var movieCount: Int {
        return movies.count
    }

    var currentPage: Int = 1

    func fetchMovies() {
        if Utlis.isInternetAvailable() {
            if currentPage == 1 {
                movies = [] 
                moviePresenterProtocol?.showLoadingIndicator()
            }
            movieService?.getNewMovies(page: currentPage) { [weak self] movies, _ in
                
                guard let movies = movies else {
                    self?.handleServiceError()
                    return
                }
                self?.handleServiceSuccess(movies: movies)
            }
        } else {
            moviePresenterProtocol?.showNoInternet()
        }
    }

    func getMovie(at index: Int) -> Movie {
        return movies[index]
    }

    func fetchLocalMovies() {
        localMovies = []
        coreDataMoviesGateway?.fetchMovies(completionHandler: { [weak self] movies, _ in
            guard let movies = movies else {
                return
            }
            self?.handleLocalFetchSuccess(movies: movies)
        })
    }

    func getLocalMovie(at index: Int) -> Movie {
        return localMovies[index]
    }

    func getSectionTitle(at section: Int) -> String {
        if getNumberOfSection() == 1 {
            return "All Movies"
        } else {
            if section == 0 {
                return "My Movies"
            } else {
                return "All Movies"
            }
        }
    }

    func getNumberOfRowsInSection(at section: Int) -> Int {
        if getNumberOfSection() == 1 {
            return movieCount
        } else {
            if section == 0 {
                return localMovieCount
            } else {
                return movieCount
            }
        }
    }

    func getMovieForCell(at indexPath: IndexPath) -> Movie {
        if getNumberOfSection() == 1 {
            return getMovie(at: indexPath.row)
        } else {
            if indexPath.section == 0 {
                return getLocalMovie(at: indexPath.row)
            } else {
                return getMovie(at: indexPath.row)
            }
        }
    }

    func getNumberOfSection() -> Int {
        if localMovieCount > 0 {
            return 2
        }
        return 1
    }

    func loadMoreMovies(for tableView: UITableView, forRowAt indexPath: IndexPath) {
        let lastSectionIndex = tableView.numberOfSections - 1
        let lastRowIndex = tableView.numberOfRows(inSection: lastSectionIndex) - 1
        if indexPath.section == lastSectionIndex && indexPath.row == lastRowIndex {
            tableView.addInfiniteScroll {
                self.loadViewData()
            }
        }
    }

    init(movieService: MovieGatewayProtocol, coreDataMoviesGateway: LocalPersistenceMoviesGateway) {
        super.init()
        self.movieService = movieService
        self.coreDataMoviesGateway = coreDataMoviesGateway
    }

    override func loadViewData() {
        super.loadViewData()
        fetchMovies()
        fetchLocalMovies()
    }

    override func handleServiceSuccess(movies: [Movie]) {
        super.handleServiceSuccess(movies: movies)
        self.movies.append(contentsOf: movies)
        currentPage = currentPage + 1
        if self.movies.count > 0 {
            moviePresenterProtocol?.refreshView()
        } else {
            moviePresenterProtocol?.addNoDataView()
        }
        debugPrint("first is \(movies.first?.title) ,page \(currentPage)")
    }
    func handleLocalFetchSuccess(movies: [Movie]) {
        self.localMovies.append(contentsOf: movies)
        self.moviePresenterProtocol?.refreshView()
    }
    override func handleServiceError() {
        super.handleServiceError()
    }
}
