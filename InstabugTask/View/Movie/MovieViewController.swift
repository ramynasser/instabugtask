//
//  ViewController.swift
//  InstabugTask
//
//  Created by Ramy Nasser on 2/19/19.
//  Copyright © 2019 Ramy Nasser. All rights reserved.
//

import UIKit

class MovieViewController: BaseVC {
    // MARK: - Outlets Variable

    @IBOutlet var movieTableView: UITableView!
    @IBOutlet var noDataView: UIView!
    @IBOutlet var noDataLabl: UILabel!

    // MARK: - Instance Variable

    fileprivate var presenter = Injector.shared.provideMoviePresenter()
    fileprivate lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(MovieViewController.viewDidLoad), for: UIControl.Event.valueChanged)
        return refreshControl
    }()

    // MARK: - view Method

    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.attachView(view: self)
        endRefreshing()
    }

    override func viewDidAppear(_: Bool) {
        presenter.fetchLocalMovies()
    }

    override func setupNavigationBar() {
        super.setupNavigationBar()
        navigationController?.navigationBar.setStyle(style: .solidNoShadow)
        title = "Movies"
        showBackButton()
    }

    override func setupTableView() {
        super.setupTableView()
        movieTableView.register(MovieCell.nib(), forCellReuseIdentifier: MovieCell.identifier)
        movieTableView.estimatedRowHeight = 50
        movieTableView.rowHeight = UITableView.automaticDimension
        movieTableView.delegate = self
        movieTableView.tableFooterView = UIView()
        movieTableView.dataSource = self
        if #available(iOS 10.0, *) {
            movieTableView.refreshControl = self.refreshControl
            movieTableView.refreshControl?.tintColor = UIColor.black
        } else {
            movieTableView.addSubview(refreshControl)
        }
    }

    func endRefreshing() {
        refreshControl.endRefreshing()
    }

    @IBAction func goToAddMovie(_: Any) {
        Navigator.navigate(to: .addMovie, from: self)
    }
}

// MARK: - Movie Presenter Protocol

extension MovieViewController: MoviePresenterProtocol {
    func addNoDataView() {
        noDataView.isHidden = false
        noDataLabl.text = "No Movies"
    }

    func showNoInternet() {
        noDataView.isHidden = false
        noDataLabl.text = "No Internet"
    }

    func refreshView() {
        DispatchQueue.main.async {
            self.movieTableView.reloadData()
        }
    }
}

// MARK: - TableView Delegate

extension MovieViewController: UITableViewDelegate {
    func numberOfSections(in _: UITableView) -> Int {
        return presenter.getNumberOfSection()
    }

    func tableView(_: UITableView, titleForHeaderInSection section: Int) -> String? {
        return presenter.getSectionTitle(at: section)
    }
}

// MARK: - TableView DataSource

extension MovieViewController: UITableViewDataSource {
    func tableView(_: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter.getNumberOfRowsInSection(at: section)
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: MovieCell.identifier, for: indexPath) as? MovieCell
        cell?.configureCell(with: presenter.getMovieForCell(at: indexPath))
        return cell ?? UITableViewCell()
    }

    func tableView(_ tableView: UITableView, willDisplay _: UITableViewCell, forRowAt indexPath: IndexPath) {
        presenter.loadMoreMovies(for: movieTableView, forRowAt: indexPath)
    }
}
