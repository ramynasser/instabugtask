//
//  AddMoviePresenter.swift
//  InstabugTask
//
//  Created by Ramy Nasser on 2/22/19.
//  Copyright © 2019 Ramy Nasser. All rights reserved.
//

import AVFoundation
import AVKit
import Foundation
import Photos

protocol AddMoviePresenterProtocol: BaseViewProtocol {
    func refreshView()
    func showSuccessMessage(message: String)
    func showErrorMessage(message: String)
    func showDatePicker()
    func openDefaultSettings()
    func openGalleryOption()
}

// MARK: - presenter Protocol

protocol AddMovieView {
    func addMovie(title: String?, overview: String?, date: String?, imageData: Data?)
    func addMovieToLocal(title: String?, overview: String?, date: String?, posterPath: String?)
    func openGallaryAccess()
}

class AddMoviePresenter: BasePresenter, AddMovieView {
    private var coreDataMoviesGateway: CoreDataMoviesGateway?
    
    private weak var movieView: AddMoviePresenterProtocol?
    var moviePresenterProtocol: AddMoviePresenterProtocol? {
        if movieView == nil {
            movieView = getView() as? AddMoviePresenterProtocol
        }
        return movieView
    }
    
    init(coreDataMoviesGateway: CoreDataMoviesGateway) {
        super.init()
        self.coreDataMoviesGateway = coreDataMoviesGateway
    }
    
    func addMovie(title: String?, overview: String?, date: String?, imageData: Data?) {
        let name = "\(String(describing: title!))\(String(describing: Date().timeIntervalSince1970))"
        if Utlis.saveImageToLocalStorage(imageData: imageData, name: name) {
            addMovieToLocal(title: title, overview: overview, date: date, posterPath: name)
        }
    }
    
    func addMovieToLocal(title: String?, overview: String?, date: String?, posterPath: String?) {
        let id: Int32 = Int32.random(in: 0 ..< Int32.max)
        let posterPath: String = posterPath ?? ""
        let title: String = title ?? ""
        let releaseDate: String = date ?? ""
        let overview: String = overview ?? ""
        let movie = Movie(id: Int(id), posterPath: posterPath,
                          title: title,
                          releaseDate: releaseDate,
                          overview: overview,
                          movieType: .myMovie)
        
        if title.isEmpty||releaseDate.isEmpty||overview.isEmpty||posterPath.isEmpty{
            self.moviePresenterProtocol?.showErrorMessage(message: ResponseMessage.noAddedData.rawValue)
        }else {
            moviePresenterProtocol?.showLoadingIndicator()
            
            coreDataMoviesGateway?.add(parameters: movie, completionHandler: { [weak self] movie, _ in
                if let _ = movie {
                    self?.moviePresenterProtocol?.refreshView()
                    self?.moviePresenterProtocol?.showSuccessMessage(message: ResponseMessage.successSaving.rawValue)
                    self?.moviePresenterProtocol?.hideLoadingIndicator()
                } else {
                    self?.moviePresenterProtocol?.showErrorMessage(message: ResponseMessage.failedAdding.rawValue)
                    self?.moviePresenterProtocol?.hideLoadingIndicator()
                }
            })
        }
    }
    
    func openGallaryAccess() {
        let status = PHPhotoLibrary.authorizationStatus()
        switch status {
        case .authorized:
            moviePresenterProtocol?.openGalleryOption()
        case .denied, .restricted:
            moviePresenterProtocol?.openDefaultSettings()
        case .notDetermined:
            PHPhotoLibrary.requestAuthorization({ [weak self] status in
                if status == PHAuthorizationStatus.authorized {
                    self?.moviePresenterProtocol?.openGalleryOption()
                }
            })
        default:
            break
        }
    }
}
