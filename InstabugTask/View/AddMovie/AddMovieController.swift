//
//  AddMovieController.swift
//  InstabugTask
//
//  Created by Ramy Nasser on 2/22/19.
//  Copyright © 2019 Ramy Nasser. All rights reserved.
//

import AVFoundation
import AVKit
import Photos
import UIKit

class AddMovieController: BaseVC {
    // MARK: - UI views

    lazy var imageView: UIImageView = {
        let imageView = UIImageView()
        imageView.image = #imageLiteral(resourceName: "camera")
        imageView.contentMode = .scaleAspectFill
        imageView.isUserInteractionEnabled = true
        imageView.clipsToBounds = true 
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()

    lazy var titleTextField: UITextField = {
        let tf = UITextField()
        tf.translatesAutoresizingMaskIntoConstraints = false
        tf.borderStyle = .roundedRect
        return tf
    }()

    lazy var titleLabel: UILabel = {
        let lb = UILabel()
        lb.translatesAutoresizingMaskIntoConstraints = false
        lb.text = "title"
        return lb
    }()

    lazy var overviewTextField: UITextView = {
        let tv = UITextView()
        tv.translatesAutoresizingMaskIntoConstraints = false
        tv.layer.borderWidth = 1
        tv.layer.cornerRadius = 8
        tv.layer.borderColor = #colorLiteral(red: 0.8549006581, green: 0.8509450555, blue: 0.8549141884, alpha: 1)
        return tv
    }()

    lazy var overviewLabel: UILabel = {
        let lb = UILabel()
        lb.translatesAutoresizingMaskIntoConstraints = false
        lb.text = "overview"
        return lb
    }()

    lazy var dateTextField: UITextField = {
        let tf = UITextField()
        tf.translatesAutoresizingMaskIntoConstraints = false
        tf.borderStyle = .roundedRect
        return tf
    }()

    lazy var dateLabel: UILabel = {
        let lb = UILabel()
        lb.translatesAutoresizingMaskIntoConstraints = false
        lb.text = "date"
        return lb
    }()

    lazy var addButton: UIButton = {
        let bt = UIButton()
        bt.translatesAutoresizingMaskIntoConstraints = false
        bt.setTitle("Add Movie", for: .normal)
        bt.setTitleColor(UIColor.white, for: .normal)
        bt.backgroundColor = .blue
        bt.layer.cornerRadius = 8
        bt.isUserInteractionEnabled = true
        return bt
    }()

    // MARK: - instance variable

    fileprivate let picker: UIImagePickerController = UIImagePickerController()
    fileprivate let datePicker = UIDatePicker()
    fileprivate var presenter = Injector.shared.provideAddMoviePresenter()

    // MARK: - controller Methods

    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.attachView(view: self)
    }

    override func setupNavigationBar() {
        super.setupNavigationBar()
        navigationController?.navigationBar.setStyle(style: .solidNoShadow)
        title = "Add Movies"
        navigationController?.navigationBar.tintColor = .black
        showBackButton()
    }

    override func setupViews() {
        super.setupViews()

        updateConstraint()

        dateTextField.delegate = self

        addButton.addTarget(self, action: #selector(addMovieAction), for: .touchUpInside)

        let gesture = UITapGestureRecognizer(target: self, action: #selector(openGallery))

        imageView.addGestureRecognizer(gesture)

        showDatePicker()
    }

    @objc func addMovieAction() {
        presenter.addMovie(title: titleTextField.text,
                           overview: overviewTextField.text,
                           date: dateTextField.text,
                           imageData: imageView.image?.pngData())
    }

    @objc func openGallery() {
        presenter.openGallaryAccess()
    }

    func updateConstraint() {
        view.addSubview(imageView)
        view.addSubview(titleTextField)
        view.addSubview(titleLabel)
        view.addSubview(overviewTextField)
        view.addSubview(overviewLabel)
        view.addSubview(dateTextField)
        view.addSubview(dateLabel)
        view.addSubview(addButton)

        imageView.widthAnchor.constraint(equalToConstant: 100).isActive = true
        imageView.heightAnchor.constraint(equalToConstant: 100).isActive = true
        imageView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        imageView.topAnchor.constraint(equalTo: view.topAnchor, constant: 20).isActive = true

        titleLabel.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 10).isActive = true
        titleTextField.leadingAnchor.constraint(equalTo: titleLabel.trailingAnchor, constant: 60).isActive = true
        titleTextField.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -20).isActive = true
        titleTextField.heightAnchor.constraint(equalToConstant: 50).isActive = true
        titleTextField.topAnchor.constraint(equalTo: imageView.bottomAnchor, constant: 15.0).isActive = true
        titleLabel.firstBaselineAnchor.constraint(equalTo: titleTextField.firstBaselineAnchor).isActive = true
        titleLabel.setContentHuggingPriority(UILayoutPriority.required, for: .horizontal)

        dateLabel.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 10).isActive = true
        dateTextField.leadingAnchor.constraint(equalTo: titleLabel.trailingAnchor, constant: 60).isActive = true
        dateTextField.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -20).isActive = true
        dateTextField.heightAnchor.constraint(equalToConstant: 50).isActive = true
        dateTextField.topAnchor.constraint(equalTo: titleTextField.bottomAnchor, constant: 15.0).isActive = true
        dateLabel.firstBaselineAnchor.constraint(equalTo: dateTextField.firstBaselineAnchor).isActive = true
        dateLabel.setContentHuggingPriority(UILayoutPriority.required, for: .horizontal)

        overviewLabel.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 10).isActive = true
        overviewTextField.leadingAnchor.constraint(equalTo: titleLabel.trailingAnchor, constant: 60).isActive = true
        overviewTextField.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -20).isActive = true
        overviewTextField.heightAnchor.constraint(equalToConstant: 100).isActive = true
        overviewTextField.topAnchor.constraint(equalTo: dateTextField.bottomAnchor, constant: 15.0).isActive = true
        overviewLabel.firstBaselineAnchor.constraint(equalTo: overviewTextField.firstBaselineAnchor).isActive = true
        overviewLabel.trailingAnchor.constraint(equalTo: overviewTextField.leadingAnchor, constant: -20).isActive = true
        overviewLabel.centerYAnchor.constraint(equalTo: overviewTextField.centerYAnchor).isActive = true

        addButton.widthAnchor.constraint(equalToConstant: 150).isActive = true
        addButton.heightAnchor.constraint(equalToConstant: 50).isActive = true
        addButton.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        addButton.topAnchor.constraint(equalTo: overviewLabel.bottomAnchor, constant: 20.0).isActive = true
    }
}

extension AddMovieController: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn _: NSRange, replacementString _: String) -> Bool {
        if textField == dateTextField, let _ = textField.text {
            return false
        } else {
            return true
        }
    }
}

// MARK: - image picker Methods

extension AddMovieController: UIImagePickerControllerDelegate, UIPopoverControllerDelegate, UINavigationControllerDelegate {
    func imagePickerControllerDidCancel(_: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }

    func imagePickerController(_: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey: Any]) {
        let chosenImage = info[UIImagePickerController.InfoKey.originalImage] as! UIImage
        imageView.image = chosenImage
        dismiss(animated: true)
    }
}

// MARK: - Add Movie presenter protocol

extension AddMovieController: AddMoviePresenterProtocol {
    func refreshView() {
        titleTextField.text = ""
        overviewTextField.text = ""
        dateTextField.text = ""
        imageView.image = #imageLiteral(resourceName: "camera")
    }

    func showSuccessMessage(message: String) {
        Utlis.showAlert(title: "Success", message: message, presenter: self)
    }

    func showErrorMessage(message: String) {
        Utlis.showAlert(title: "Error", message: message, presenter: self)
    }

    func openGalleryOption() {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.photoLibrary) {
            picker.allowsEditing = false
            picker.delegate = self
            picker.sourceType = UIImagePickerController.SourceType.photoLibrary
            present(picker, animated: true, completion: nil)
        }
    }

    func openDefaultSettings() {
        let title: String = "Access permission"
        let message: String = "App needs permission to access the gallery. press 'Settings' to change"
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Settings", style: .default) { _ in
            guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else {
                return
            }
            if UIApplication.shared.canOpenURL(settingsUrl) {
                UIApplication.shared.open(settingsUrl)
            }

        })
        let declineAction = UIAlertAction(title: "Cancel", style: .cancel)
        alert.addAction(declineAction)
        present(alert, animated: true, completion: nil)
    }
}

// MARK: - helper Method

extension AddMovieController {
    func showDatePicker() {
        // Formate Date
        datePicker.datePickerMode = .date

        // ToolBar
        let toolbar = UIToolbar()
        toolbar.sizeToFit()
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(doneDatePicker))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelDatePicker))
        toolbar.setItems([doneButton, spaceButton, cancelButton], animated: false)

        dateTextField.inputAccessoryView = toolbar
        dateTextField.inputView = datePicker
    }

    @objc func doneDatePicker() {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        dateTextField.text = formatter.string(from: datePicker.date)
        view.endEditing(true)
    }

    @objc func cancelDatePicker() {
        view.endEditing(true)
    }
}
