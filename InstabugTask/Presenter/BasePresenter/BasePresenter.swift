//
//  BasePresenter.swift
//  InstabugTask
//
//  Created by Ramy Nasser on 1/29/19.
//  Copyright © 2019 Ramy Nasser. All rights reserved.
//

import Foundation
class BasePresenter: BasePresenterProtocol, ServiceSubscriberProtocol {
    private weak var view: BaseViewProtocol?

    // MARK: ViewProtocol methods

    func attachView(view: BaseViewProtocol) {
        self.view = view
        loadViewData()
    }

    func getView() -> BaseViewProtocol? {
        return view
    }

    func isViewAttached() -> Bool {
        return view == nil
    }

    func detachView() {
        view = nil
    }

    func loadViewData() {}

    // MARK: ServiceSubscriberProtocol methods

    func handleServiceError() {
        view?.hideLoadingIndicator()
    }

    func handleServiceSuccess(movies _: [Movie]) {
        view?.hideLoadingIndicator()
    }
}
