//
//  BaseViewProtocol.swift
//  InstabugTask
//
//  Created by Ramy Nasser on 1/29/19.
//  Copyright © 2019 Ramy Nasser. All rights reserved.
//

import UIKit
protocol BaseViewProtocol: class {
    func hideLoadingIndicator()
    func showLoadingIndicator()
    func showMessageToast(message: String?)
}
