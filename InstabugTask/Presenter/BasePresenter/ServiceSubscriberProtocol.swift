//
//  VFServiceSubscriberProtocol.swift
//  InstabugTask
//
//  Created by Ramy Nasser on 1/29/19.
//  Copyright © 2019 Ramy Nasser. All rights reserved.
//

import Foundation

protocol ServiceSubscriberProtocol {
    /**
     * Service will notify the subscriber on error and Success here.
     */
    func handleServiceError()
    func handleServiceSuccess(movies: [Movie])
}
