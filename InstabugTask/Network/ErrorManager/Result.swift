//
//  ResultResponse.swift
//  InstabugTask
//
//  Created by Ramy Nasser on 2/19/19.
//  Copyright © 2019 Ramy Nasser. All rights reserved.
//

import Foundation

enum Result<ErrorModel> {
    case success
    case failure(ErrorModel)
}
