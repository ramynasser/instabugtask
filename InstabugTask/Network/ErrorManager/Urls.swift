//
//  Urls.swift
//  InstabugTask
//
//  Created by Ramy Nasser on 2/19/19.
//  Copyright © 2019 Ramy Nasser. All rights reserved.
//

// this enum ,
// i use it to handle many application shema ,
// if i make target for production and development and for stagging
import Foundation
enum Urls {
    enum keys {
        case movie

        var absoluteValue: String {
            switch self {
            case .movie:
                return "acea91d2bff1c53e6604e4985b6989e2"
            }
        }
    }

    enum Paths {
        case getMove

        var absolutePath: String {
            switch self {
            case .getMove:
                return "movie"
            }
        }
    }

    static var environment: UrlEnvironment {
        #if DEVELOPMENT
            return .development
        #elseif PRODUCTION
            return .production
        #else
            return .staging
        #endif
    }

    enum UrlEnvironment: String {
        case development
        case production
        case staging

        var baseUrl: String {
            let developmentUrl = "https://api.themoviedb.org/3/discover/"
            let productionUrl = "https://api.themoviedb.org/3/discover/"
            let stagingUrl = "https://api.themoviedb.org/3/discover/"

            switch self {
            case .development:
                return developmentUrl
            case .production:
                return productionUrl
            case .staging:
                return stagingUrl
            }
        }

        var mediaUrl: String {
            let developmentUrl = "https://image.tmdb.org/t/p/w185"
            let productionUrl = "https://image.tmdb.org/t/p/w185"
            let stagingUrl = "https://image.tmdb.org/t/p/w185"

            switch self {
            case .development:
                return developmentUrl
            case .production:
                return productionUrl
            case .staging:
                return stagingUrl
            }
        }
    }
}
