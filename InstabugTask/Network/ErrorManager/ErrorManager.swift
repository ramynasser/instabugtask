//
//  ErrorManager.swift
//  InstabugTask
//
//  Created by Ramy Nasser on 2/20/19.
//  Copyright © 2019 Ramy Nasser. All rights reserved.
//

import Foundation
struct ErrorManager {
    static func handleResponseMessage(_ response: HTTPURLResponse) -> Result<String> {
        switch response.statusCode {
        case 200 ... 299: return .success
        case 401 ... 500: return .failure(ResponseMessage.authenticationError.rawValue)
        case 501 ... 599: return .failure(ResponseMessage.badRequest.rawValue)
        case 600: return .failure(ResponseMessage.outdated.rawValue)
        case -1002: return .failure(ResponseMessage.noInternet.rawValue)
        default: return .failure(ResponseMessage.failed.rawValue)
        }
    }
}
