//
//  ResponseMessage.swift
//  InstabugTask
//
//  Created by Ramy Nasser on 2/19/19.
//  Copyright © 2019 Ramy Nasser. All rights reserved.
//

import Foundation

enum ResponseMessage: String {
    case success
    case authenticationError = "You need to be authenticated first."
    case badRequest = "Bad request"
    case outdated = "The url you requested is outdated."
    case failed = "Network request failed."
    case noData = "Response returned with no data to decode."
    case unableToDecode = "We could not decode the response."
    case noInternet = "Please check your network connection."
    case failedRetrieving = "Failed retrieving movies the data base"
    case failedAdding = "Failed adding the movie in the data base"
    case failedSaving = "Failed saving the context"
    case successSaving = "saving to the database successfully"
    case noAddedData = "Empty fields"
}

enum StatusCode: Int {
    case success = 200
    case badRequest = 400
    case unauthenticated = 401
    case forbidden = 403
    case notFound = 404
    case notAcceptable = 406
    case unsupportedMediaType = 415
    case tooManyRequests = 429
    case serverError = 500
    case unprocessableEntity = 422
    case timeOut = 499
    case complete = 201
}
