//
//  MovieEndPoint.swift
//  InstabugTask
//
//  Created by Ramy Nasser on 2/20/19.
//  Copyright © 2019 Ramy Nasser. All rights reserved.
//

import Foundation
public enum MovieEndPoint {
    case getMovies(page: Int)
}

extension MovieEndPoint: EndPointType {
    var baseURL: URL {
        guard let url = URL(string: Urls.environment.baseUrl) else {
            fatalError("baseURL could not be configured.")
        }
        return url
    }

    var path: Urls.Paths {
        return .getMove
    }

    var httpMethod: HTTPMethod {
        return .get
    }

    var task: HTTPTask {
        switch self {
        case let .getMovies(page):
            return .requestParameters(bodyParameters: nil,
                                      bodyEncoding: .urlEncoding,
                                      urlParameters: ["page": page,
                                                      "api_key": Urls.keys.movie.absoluteValue])
        }
    }

    var headers: HTTPHeaders? {
        return nil
    }
}
