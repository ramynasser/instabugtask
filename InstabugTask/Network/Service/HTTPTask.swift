//
//  HTTPTask.swift
//  InstabugTask
//
//  Created by Ramy Nasser on 2/19/19.
//  Copyright © 2019 Ramy Nasser. All rights reserved.
//

import Foundation
public typealias HTTPHeaders = [String: String]

public enum HTTPTask {
    case request // normal request without any parameter

    case requestParameters(bodyParameters: Parameters?,
                           bodyEncoding: ParameterEncoding,
                           urlParameters: Parameters?) // request with body and url parameter

    case requestParametersAndHeaders(bodyParameters: Parameters?,
                                     bodyEncoding: ParameterEncoding,
                                     urlParameters: Parameters?,
                                     additionHeaders: HTTPHeaders?) // request with body and url and headers
}
