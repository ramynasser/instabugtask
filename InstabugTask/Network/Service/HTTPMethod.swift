//
//  HTTPMethod.swift
//  InstabugTask
//
//  Created by Ramy Nasser on 2/19/19.
//  Copyright © 2019 Ramy Nasser. All rights reserved.
//

import Foundation

public enum HTTPMethod: String {
    case get = "GET"
    case post = "POST"
    case put = "PUT"
    case patch = "PATCH"
    case delete = "DELETE"
}
