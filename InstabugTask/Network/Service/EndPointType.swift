//
//  EndPointType.swift
//  InstabugTask
//
//  Created by Ramy Nasser on 2/19/19.
//  Copyright © 2019 Ramy Nasser. All rights reserved.
//

import Foundation
// use this protocol to build your request in network manager
protocol EndPointType {
    var baseURL: URL { get }
    var path: Urls.Paths { get }
    var httpMethod: HTTPMethod { get }
    var task: HTTPTask { get }
    var headers: HTTPHeaders? { get }
}
