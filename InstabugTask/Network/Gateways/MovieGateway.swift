//
//  NetworkManager.swift
//  InstabugTask
//
//  Created by Ramy Nasser on 2/20/19.
//  Copyright © 2019 Ramy Nasser. All rights reserved.
//

import Foundation
protocol MovieGatewayProtocol{
     func getNewMovies(page: Int, completion: @escaping FetchMoviesEntityCompletionHandler)
}
struct MovieGateway: MovieGatewayProtocol {
    fileprivate let router = NetworkManager<MovieEndPoint>()

    func getNewMovies(page: Int, completion: @escaping FetchMoviesEntityCompletionHandler) {
        router.request(.getMovies(page: page)) { data, response, error in

            if error != nil {
                completion(nil, ResponseMessage.noInternet.rawValue)
            }

            if let response = response as? HTTPURLResponse {
                let result = ErrorManager.handleResponseMessage(response)
                switch result {
                case .success:
                    guard let responseData = data else {
                        completion(nil, ResponseMessage.noData.rawValue)
                        return
                    }
                    do {
                        let apiResponse = try JSONDecoder().decode(MovieApiResponse.self, from: responseData)
                        if apiResponse.numberOfPages >= page {
                            completion(apiResponse.movies, nil)
                        }else {
                            completion([], nil)
                        }
                    } catch {
                        print(error)
                        completion(nil, ResponseMessage.unableToDecode.rawValue)
                    }
                case let .failure(networkFailureError):
                    completion(nil, networkFailureError)
                }
            }
        }
    }
}
