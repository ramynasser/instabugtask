//
//  CoreDataStack.swift
//  InstabugTask
//
//  Created by Ramy Nasser on 2/22/19.
//  Copyright © 2019 Ramy Nasser. All rights reserved.
//

import CoreData
import Foundation

protocol CoreDataStack {
    var items: [Movie]{get}
    func add(item: Movie)
    func getItems()->[Movie]
    func remove(item _: Movie, index: Int)
    func getItem(withIndex index: Int) -> Movie
    func getItem(withId id: Int) -> Movie?
    func remove(withIndex index: Int)
    func clearStack()
}

class CoreDataStackImplementation: CoreDataStack {
    
    static let shared: CoreDataStack = CoreDataStackImplementation()
    var items: [Movie] = []
    
    func add(item: Movie) {
        items.append(item)
    }
    
    func remove(item _: Movie, index: Int) {
        items.remove(at: index)
    }
    
    func getItem(withIndex index: Int) -> Movie {
        return items[index]
    }
    func getItem(withId id: Int) -> Movie? {
        return items.filter{$0.id==id}.first
    }
    
    func remove(withIndex index: Int) {
        items.remove(at: index)
    }
    func getItems() -> [Movie] {
        return items
    }
    
    func clearStack() {
        items = []
    }
    
}
