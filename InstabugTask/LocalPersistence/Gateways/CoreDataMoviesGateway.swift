//
//  CoreDataMoviesGateway.swift
//  InstabugTask
//
//  Created by Ramy Nasser on 2/22/19.
//  Copyright © 2019 Ramy Nasser. All rights reserved.
//

import CoreData
import Foundation

import Foundation

typealias FetchMoviesEntityCompletionHandler = (_ movies: [Movie]?, _ error: String?) -> Void
typealias AddMovieEntityGatewayCompletionHandler = (_ movie: Movie?, _ error: String?) -> Void

protocol LocalPersistenceMoviesGateway {
    func fetchMovies(completionHandler: @escaping FetchMoviesEntityCompletionHandler)
    func add(parameters: Movie, completionHandler: @escaping AddMovieEntityGatewayCompletionHandler)
}

class CoreDataMoviesGateway: LocalPersistenceMoviesGateway {
    let viewContext: CoreDataStack

    init(viewContext: CoreDataStack) {
        self.viewContext = viewContext
    }

    // MARK: - MoviesGateway

    func fetchMovies(completionHandler: @escaping FetchMoviesEntityCompletionHandler) {
       let movies = viewContext.getItems()
        completionHandler(movies, nil)
    }

    func add(parameters: Movie, completionHandler: @escaping AddMovieEntityGatewayCompletionHandler) {

        viewContext.add(item: parameters)
        guard let coreDataMovie = viewContext.getItem(withId: parameters.id) else {
            completionHandler(nil, ResponseMessage.failedAdding.rawValue)
            return
        }
        completionHandler(coreDataMovie, nil)
    }
}
