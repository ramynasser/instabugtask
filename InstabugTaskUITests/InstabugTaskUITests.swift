//
//  InstabugTaskUITests.swift
//  InstabugTaskUITests
//
//  Created by Ramy Nasser on 2/19/19.
//  Copyright © 2019 Ramy Nasser. All rights reserved.
//

import XCTest

class InstabugTaskUITests: XCTestCase {

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.

        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false

        // UI tests must launch the application that they test. Doing this in setup will make sure it happens for each test method.
        XCUIApplication().launch()

        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testExample() {
        // Use recording to get started writing UI tests.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }
    
    func testOpenGallery() {
        XCUIApplication().otherElements.containing(.navigationBar, identifier:"Add Movies").children(matching: .other).element.children(matching: .other).element.children(matching: .other).element.children(matching: .image).element.tap()
        

    }
    func clickOnErrorAlert() {
        let app = XCUIApplication()
        let addMovieButton = app.buttons["Add Movie"]
        addMovieButton.tap()
        
        let cancelButton = app.alerts["Error"].buttons["Cancel"]
        cancelButton.tap()
        app.otherElements.containing(.navigationBar, identifier:"Add Movies").children(matching: .other).element.children(matching: .other).element.children(matching: .other).element.tap()
        addMovieButton.tap()
        cancelButton.tap()

    }
    func testTableViewScroll() {
        XCUIApplication().tables.staticTexts["Creed II"].swipeUp()
    }
    func testClickGoToAdd() {
        // Use recording to get started writing UI tests.
        
        let app = XCUIApplication()
        let tablesQuery = app.tables
        tablesQuery/*@START_MENU_TOKEN@*/.staticTexts["2018-11-23"]/*[[".cells.staticTexts[\"2018-11-23\"]",".staticTexts[\"2018-11-23\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.swipeDown()
        
        let theFavouriteStaticText = tablesQuery/*@START_MENU_TOKEN@*/.staticTexts["The Favourite"]/*[[".cells.staticTexts[\"The Favourite\"]",".staticTexts[\"The Favourite\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/
        theFavouriteStaticText.tap()
        theFavouriteStaticText.swipeDown()
        
        let addMoviesNavigationBar = app.navigationBars["Add Movies"]
        addMoviesNavigationBar.tap()
        addMoviesNavigationBar.buttons["Movies"].tap()
        
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }
    
    func testClickAdd() {
        let app = XCUIApplication()
        app.navigationBars["Photos"].buttons["Cancel"].tap()
        app.buttons["Add Movie"].tap()
    }


}
