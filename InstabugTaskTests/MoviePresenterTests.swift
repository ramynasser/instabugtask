////
////  MoviePresenterTests.swift
////  InstabugTaskTests
////
////  Created by Ramy Nasser on 2/23/19.
////  Copyright © 2019 Ramy Nasser. All rights reserved.
////
//
import XCTest
@testable import InstabugTask

class MoviePresenterTests: XCTestCase {
    var moviePresenter: MoviePresenter!
    var service = MockMovieGateway()
    var localGateway = MockLocalMovieGateway()
    override func setUp() {
        super.setUp()
        moviePresenter = MoviePresenter(movieService: service, coreDataMoviesGateway: localGateway)
    }
    
    func test_fetchMoviesFromPresenter_success() {
        moviePresenter.fetchMovies()
        XCTAssertEqual(self.moviePresenter.movies.count, 4)
    }
    
    func test_fetchMoviesFromPresenter_failed() {
        let presenter = MoviePresenter(movieService: MockFailedMovieGateway(), coreDataMoviesGateway: localGateway)
        moviePresenter.fetchMovies()
        moviePresenter.handleServiceError()
        XCTAssertEqual(presenter.movies, [])
    }
    
    func test_fetchMovies_success() {
        let executeCompletionHandlerExpectation = expectation(description: "FetchMovies success expectation")
        service.getNewMovies(page: 1) { (movies, _ ) in
            self.moviePresenter.handleServiceSuccess(movies: movies ?? [])
            XCTAssertEqual(self.moviePresenter.movies.count, movies?.count)
            XCTAssertEqual(self.moviePresenter.currentPage, 2)
            XCTAssertNotNil(movies)
            executeCompletionHandlerExpectation.fulfill()
        }
        waitForExpectations(timeout: 1, handler: nil)
    }
    func test_fetchMovies_Failure() {
        let executeCompletionHandlerExpectation = expectation(description: "FetchMovies Failure expectation")
        MockFailedMovieGateway().getNewMovies(page: 1) { (movies,error) in
            XCTAssertEqual(self.moviePresenter.movies.count,0)
            XCTAssertEqual(self.moviePresenter.currentPage, 1)
            XCTAssertNotNil(error)
            executeCompletionHandlerExpectation.fulfill()
        }
        waitForExpectations(timeout: 1, handler: nil)
    }
    
    func test_getSectionTitle_whenHaveLocalMovies() {
        let executeCompletionHandlerExpectation = expectation(description: "getSectionTitle when Have Local Movies expectation")
        service.getNewMovies(page: 1) { (movies, _ ) in
            self.moviePresenter.handleServiceSuccess(movies:[])
        }
        localGateway.fetchMovies { (movies, _ ) in
            executeCompletionHandlerExpectation.fulfill()
            self.moviePresenter.handleLocalFetchSuccess(movies: movies ?? [] )
            XCTAssertEqual(self.moviePresenter.getNumberOfSection(), 2)
            XCTAssertEqual(self.moviePresenter.getSectionTitle(at:0), "My Movies")
            XCTAssertEqual(self.moviePresenter.getSectionTitle(at:1), "All Movies")

        }
        waitForExpectations(timeout: 1, handler: nil)
    }
    
    func test_getSectionTitle_whenNoLocalMovies() {
        let executeCompletionHandlerExpectation = expectation(description: "getSectionTitle when No Local Movies expectation")
        self.moviePresenter.handleLocalFetchSuccess(movies:[] )
        service.getNewMovies(page: 1) { (movies, _ ) in
            executeCompletionHandlerExpectation.fulfill()
            self.moviePresenter.handleServiceSuccess(movies:[])
            XCTAssertEqual(self.moviePresenter.getNumberOfSection(), 1)
            XCTAssertEqual(self.moviePresenter.getSectionTitle(at:0), "All Movies")
        }
        waitForExpectations(timeout: 1, handler: nil)
    }
    
    func test_getNumberOfRowsInSection_whenNoLocalMovies() {
        let executeCompletionHandlerExpectation = expectation(description: "getMovieForCell whenNoLocalMovies expectation")
        self.moviePresenter.handleLocalFetchSuccess(movies:[] )
        service.getNewMovies(page: 1) { (movies, _ ) in
            executeCompletionHandlerExpectation.fulfill()
            self.moviePresenter.handleServiceSuccess(movies:movies ?? [])
            XCTAssertEqual(self.moviePresenter.getMovieForCell(at: IndexPath(row: 0, section: 1)),movies?.first!)
            
        }
        waitForExpectations(timeout: 1, handler: nil)
    }
    
    func test_getNumberOfRowsInSection() {
        let LocalMovies = [
            Movie(id: 5, posterPath: "", title: "", releaseDate: "", overview: "", movieType: .allMovie),          Movie(id: 5, posterPath: "", title: "", releaseDate: "", overview: "", movieType: .allMovie),
            Movie(id: 5, posterPath: "", title: "", releaseDate: "", overview: "", movieType: .allMovie),            Movie(id: 5, posterPath: "", title: "", releaseDate: "", overview: "", movieType: .allMovie)
            
        ]
        let executeCompletionHandlerExpectation = expectation(description: "getNumberOfRowsInSection")
        self.moviePresenter.handleLocalFetchSuccess(movies:LocalMovies)
        service.getNewMovies(page: 1) { (movies, _ ) in
            executeCompletionHandlerExpectation.fulfill()
            self.moviePresenter.handleServiceSuccess(movies:movies ?? [])
            XCTAssertEqual(self.moviePresenter.getNumberOfRowsInSection(at: 0),self.moviePresenter.localMovieCount)
            XCTAssertEqual(self.moviePresenter.getNumberOfRowsInSection(at: 1),self.moviePresenter.movieCount)
        }
        waitForExpectations(timeout: 1, handler: nil)

    }
    func test_getMovieForCell_whenNoLocalMovies() {
        let executeCompletionHandlerExpectation = expectation(description: "getMovieForCell whenNoLocalMovies expectation")
        self.moviePresenter.handleLocalFetchSuccess(movies:[] )
        service.getNewMovies(page: 1) { (movies, _ ) in
            executeCompletionHandlerExpectation.fulfill()
            self.moviePresenter.handleServiceSuccess(movies:movies ?? [])
            XCTAssertEqual(self.moviePresenter.getMovieForCell(at: IndexPath(row: 0, section: 1)),movies?.first!)
            
        }
        waitForExpectations(timeout: 1, handler: nil)
    }
    
    func test_getMovieForCell_WhenHaveLocalMovies() {
        let LocalMovies = [
            Movie(id: 5, posterPath: "", title: "", releaseDate: "", overview: "", movieType: .allMovie),          Movie(id: 5, posterPath: "", title: "", releaseDate: "", overview: "", movieType: .allMovie),
            Movie(id: 5, posterPath: "", title: "", releaseDate: "", overview: "", movieType: .allMovie),            Movie(id: 5, posterPath: "", title: "", releaseDate: "", overview: "", movieType: .allMovie)
            
        ]
        let executeCompletionHandlerExpectation = expectation(description: "getMovieForCell WhenHaveLocalMovies")
        self.moviePresenter.handleLocalFetchSuccess(movies:LocalMovies)
        service.getNewMovies(page: 1) { (movies, _ ) in
            executeCompletionHandlerExpectation.fulfill()
            self.moviePresenter.handleServiceSuccess(movies:movies ?? [])
            XCTAssertEqual(self.moviePresenter.getMovieForCell(at: IndexPath(row: 0, section: 0)),LocalMovies.first!)
            XCTAssertEqual(self.moviePresenter.getMovieForCell(at: IndexPath(row: 0, section: 1)),movies?.first!)
        }
        waitForExpectations(timeout: 1, handler: nil)
    }
    
//    func getMovieForCell(at indexPath: IndexPath) -> Movie {
//        if getNumberOfSection() == 1 {
//            return getMovie(at: indexPath.row)
//        } else {
//            if indexPath.section == 0 {
//                return getLocalMovie(at: indexPath.row)
//            } else {
//                return getMovie(at: indexPath.row)
//            }
//        }
//    }
//        if getNumberOfSection() == 1 {
//            return movieCount
//        } else {
//            if section == 0 {
//                return localMovieCount
//            } else {
//                return movieCount
//            }
//        }
//    }
//
//    func getMovieForCell(at indexPath: IndexPath) -> Movie {
//        if getNumberOfSection() == 1 {
//            return getMovie(at: indexPath.row)
//        } else {
//            if indexPath.section == 0 {
//                return getLocalMovie(at: indexPath.row)
//            } else {
//                return getMovie(at: indexPath.row)
//            }
//        }
//    }
//
        
    
}

struct MockMovieGateway: MovieGatewayProtocol {
    func getNewMovies(page: Int, completion: @escaping FetchMoviesEntityCompletionHandler) {
        let movies = [
            Movie(id: 5, posterPath: "", title: "", releaseDate: "", overview: "", movieType: .allMovie),          Movie(id: 5, posterPath: "", title: "", releaseDate: "", overview: "", movieType: .allMovie),
            Movie(id: 5, posterPath: "", title: "", releaseDate: "", overview: "", movieType: .allMovie),            Movie(id: 5, posterPath: "", title: "", releaseDate: "", overview: "", movieType: .allMovie)
            
        ]
        completion(movies,nil)
    }
}
struct MockFailedMovieGateway: MovieGatewayProtocol {
    func getNewMovies(page: Int, completion: @escaping FetchMoviesEntityCompletionHandler) {
        completion(nil,ResponseMessage.failed.rawValue)
    }
}

struct MockLocalMovieGateway: LocalPersistenceMoviesGateway {
    func fetchMovies(completionHandler: @escaping FetchMoviesEntityCompletionHandler) {
        let movies = [
            Movie(id: 5, posterPath: "", title: "", releaseDate: "", overview: "", movieType: .allMovie),
            Movie(id: 5, posterPath: "", title: "", releaseDate: "", overview: "", movieType: .allMovie),
            Movie(id: 5, posterPath: "", title: "", releaseDate: "", overview: "", movieType: .allMovie),
            Movie(id: 5, posterPath: "", title: "", releaseDate: "", overview: "", movieType: .allMovie)
        ]
        completionHandler(movies,nil)
    }
    
    func add(parameters: Movie, completionHandler: @escaping AddMovieEntityGatewayCompletionHandler) {
        
    }
    
    
}
