//
//  NetworkLayerTests.swift
//  InstabugTaskTests
//
//  Created by Ramy Nasser on 2/23/19.
//  Copyright © 2019 Ramy Nasser. All rights reserved.
//

import Foundation
import XCTest
@testable import InstabugTask

class NetworkLayerTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testURLEncoding() {
        guard let url = URL(string: "https:www.google.com/") else {
            XCTAssertTrue(false, "Could not instantiate url")
            return
        }
        var urlRequest = URLRequest(url: url)
        let parameters: Parameters = [
            "id": 1,
            "Name": "ramy",
            "Email": "ramynasser95@gmail.com"
        ]
        
        do {
            let encoder = URLParameterEncoder()
            try encoder.encode(urlRequest: &urlRequest, with: parameters)
            guard let fullURL = urlRequest.url else {
                XCTAssertTrue(false, "urlRequest url is nil.")
                return
            }
            
            let expectedURL = "https:www.google.com/?Name=ramy&Email=ramynasser95%2540gmail.com&id=1"
            XCTAssertEqual(fullURL.absoluteString.sorted(), expectedURL.sorted())
        }catch {
            
        }
    }
    
    
}
