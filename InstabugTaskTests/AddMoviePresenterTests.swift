//
//  AddMoviePresenterTests.swift
//  InstabugTaskTests
//
//  Created by Ramy Nasser on 2/23/19.
//  Copyright © 2019 Ramy Nasser. All rights reserved.
//

import Foundation
import XCTest
@testable import InstabugTask

class AddMoviePresenterTests: XCTestCase {
    var addMoviePresenter: AddMoviePresenter!
    var fakeSuccessLocalGateway = AddMovieSuccessGateway(viewContext: CoreDataStackImplementation.shared)
    var fakeFailedLocalGateway = AddMovieSuccessGateway(viewContext: CoreDataStackImplementation.shared)

    var moviePresenter: MoviePresenter!
    var service = MockMovieGateway()

    
    override func setUp() {
        super.setUp()
        addMoviePresenter = AddMoviePresenter(coreDataMoviesGateway: fakeSuccessLocalGateway)
        moviePresenter = MoviePresenter(movieService: service, coreDataMoviesGateway: fakeSuccessLocalGateway)
    }
    
    func testAddToLocal_whenSuccess() {
        addMoviePresenter.addMovieToLocal(title: "", overview: "", date: "", posterPath: "")
        moviePresenter.fetchLocalMovies()
        XCTAssertEqual(moviePresenter.localMovieCount, 4)
    }
    
    
    
    
}
class AddMovieSuccessGateway: CoreDataMoviesGateway {
    override func fetchMovies(completionHandler: @escaping FetchMoviesEntityCompletionHandler) {
        let movies = [
            Movie(id: 5, posterPath: "", title: "", releaseDate: "", overview: "", movieType: .allMovie),
            Movie(id: 5, posterPath: "", title: "", releaseDate: "", overview: "", movieType: .allMovie),
            Movie(id: 5, posterPath: "", title: "", releaseDate: "", overview: "", movieType: .allMovie),
            Movie(id: 5, posterPath: "", title: "", releaseDate: "", overview: "", movieType: .allMovie)
        ]
        completionHandler(movies,nil)
    }
    
    override func add(parameters: Movie, completionHandler: @escaping AddMovieEntityGatewayCompletionHandler) {
        CoreDataStackImplementation.shared.add(item: parameters)
        completionHandler(parameters, nil)
    }
    
    
    
}
class AddMovieGateway: CoreDataMoviesGateway {
    override func fetchMovies(completionHandler: @escaping FetchMoviesEntityCompletionHandler) {
        completionHandler(nil,"error")
    }
    
    override func add(parameters: Movie, completionHandler: @escaping AddMovieEntityGatewayCompletionHandler) {
        CoreDataStackImplementation.shared.add(item: parameters)
        completionHandler(nil, nil)
    }
    
    
    
}
