# MovieApp - iOS - (MVP + Navigator Demo)

### Description
*InstabugTask* is an iOS application built to displays list of movies from the internet , and allow the user to add movies of their own .

### Run Requirements

* Xcode 10.1
* Swift 4.2

### Notes 
* for Network layer using `URLSession` .
* for Local Layer using `stack implement` .
* for UI Views using `storyboard`  and  `Anchor` .

### High Level Layers
* `View` - delegates user interaction events to the `Presenter` and displays data passed by the `Presenter`
    * All `UIViewController`, `UIView`, `UITableViewCell` subclasses belong to the `View` layer
    * Usually the view is passive  - it shouldn't contain any complex logic and that's why most of the times we don't need write Unit Tests for it
* `Presenter` - contains the presentation logic and tells the `View` what to present
    * Usually we have one `Presenter` per scene (view controller)
    * It doesn't reference the concrete type of the `View`, but rather it references the `View` protocol that is implemented usually by a `UIViewController` subclass
    * It should be covered by Unit Tests
* `injector` - injects the dependency object 
    * Usually it contains very simple logic and we don't need to write Unit Tests for it
* `Navigator` - contains navigation / flow logic from one scene (view controller) to another
    * It needs to reference it in the view controller , we can also reference to it from presenter
* `Gateway` - contains actual implementation of the protocols defined in the `Application Logic` layer
    * We can implement for instance an `MovieGatewayProtocol` protocol using `URLSession` 
    * We can implement for instance a `LocalPersistenceMoviesGateway` protocol using `stack`
    * It should be covered by Unit Tests
* `Model` - plain `Swift` classes / structs
    * Models objects used by your application such as `Movie`, etc
    
#### Unit Tests
* unit test for presenters and Gateway
* ui test
